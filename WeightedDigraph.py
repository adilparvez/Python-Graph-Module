from NodeException import *
from EdgeException import *

class WeightedDigraph:

    def __init__(self, adjacencyLists = None):
        if adjacencyLists is None:
            self.nodes = []
            self.adjacencyLists = {}
        else:
            self.nodes = list(adjacencyLists.keys())
            self.adjacencyLists = adjacencyLists

    def __repr__(self):
        return str(self.adjacencyLists)

    def addNode(self, node):
        if self.containsNode(node):
            raise NodeException("Node already present in graph.")
        self.nodes.append(node)
        self.adjacencyLists.update({node: {}})

    def containsNode(self, node):
        return node in self.nodes

    def deleteNode(self, node):
        if self.containsNode(node):
            self.nodes.remove(node)
            del self.adjacencyLists[node]

    def getNodes(self):
        return self.nodes

    def addEdge(self, start, end, weight):
        if not self.containsNode(start):
            raise EdgeException("Start node not in graph.")
        elif not self.containsNode(end):
            raise EdgeException("End node not in graph.")
        elif start == end:
            raise NodeException("Start node and end node not distinct.")
        self.adjacencyLists[start][end] = weight

    def containsEdge(self, start, end):
        if not self.containsNode(start):
            raise EdgeException("Start node not in graph.")
        elif not self.containsNode(end):
            raise EdgeException("End node not in graph.")
        elif start == end:
            raise NodeException("Start node and end node not distinct.")
        return end in self.getNeighbours(start)

    def deleteEdge(self, start, end):
        if not self.containsNode(start):
            raise EdgeException("Start node not in graph.")
        elif not self.containsNode(end):
            raise EdgeException("End node not in graph.")
        elif start == end:
            raise NodeException("Start node and end node not distinct.")
        del self.adjacencyLists[start][end]

    def getNeighbours(self, node):
        if not self.containsNode(node):
            raise NodeException("Node not in graph.")
        return list(self.adjacencyLists[node].keys())

    def getWeight(self, start, end):
        if not(self.containsNode(start) and self.containsNode(end)):
            raise EdgeException("Edge not in graph.")
        if start == end:
            raise NodeException("Start node and end node not distinct.")
        if end not in self.getNeighbours(start):
            raise EdgeException("Edge not in graph.")
        return self.adjacencyLists[start][end]

    def getEdges(self):
        edges = []
        for u in self.nodes:
            for v in self.getNeighbours(u):
                edges.append((u, v, self.getWeight(u, v)))
        return edges

    def dijkstra(self, source, target = None):
        if not self.containsNode(source):
            raise NodeException("Source node not in graph.")
        if target != None and not self.containsNode(target):
            raise NodeException("Target node not in graph.")
        distances = {source: 0}
        predecessors = {}
        q = []
        for v in self.nodes:
            if v != source:
                distances[v] = float('inf')
                predecessors[v] = None
            q.append(v)

        while q:
            u = q[0]
            for v in q:
                if distances[v] < distances[u]:
                    u = v
            q.remove(u)

            for v in self.getNeighbours(u):
                d = distances[u] + self.getWeight(u, v)
                if d < distances[v]:
                    distances[v] = d
                    predecessors[v] = u

        if target is None:
            return distances, predecessors

        path = []
        distance = distances[target]
        while True:
            path.append(target)
            if target == source:
                break
            target = predecessors[target]
        return distance, path[::-1]

    def bellmanFord(self, source):
        if not self.containsNode(source):
            raise NodeException("Source node not in graph.")
        containsNegativeCycle = False
        distances = {source: 0}
        predecessors = {}

        for v in self.nodes:
            if v != source:
                distances[v] = float('inf')
                predecessors[v] = None

        for i in range(len(self.nodes) - 1):
            for u in self.nodes:
                for v in self.getNeighbours(u):
                    d = distances[u] + self.getWeight(u, v)
                    if d < distances[v]:
                        distances[v] = d
                        predecessors[v] = u

        for u in self.nodes:
            for v in self.nodes:
                if v != u and self.containsEdge(u, v) and distances[v] > distances[u] + self.getWeight(u, v):
                    containsNegativeCycle = True

        return distances, predecessors, containsNegativeCycle

    def dfs(self, start):
        if not self.containsNode(start):
            raise NodeException("Start node not in graph.")
        visited = []
        stack = []
        stack.append(start)

        while stack:
            u = stack.pop()    
            if u not in visited:
                visited.append(u)
                neighbours = {node: self.getWeight(u, node) for node in self.getNeighbours(u)}
                while neighbours:
                    v = max(neighbours, key = lambda x: neighbours[x])
                    del neighbours[v]
                    if v not in visited:
                        stack.append(v)

        return visited

    def bfs(self, start):
        if not self.containsNode(start):
            raise NodeException("Start node not in graph.")
        visited = [start]
        frontier = [start]
        level = 1

        while frontier:
            next = []
            for u in frontier:
                neighbours = {node: self.getWeight(u, node) for node in self.getNeighbours(u)}
                while neighbours:
                    v = min(neighbours, key = lambda x: neighbours[x])
                    del neighbours[v]
                    if v not in visited:
                        visited.append(v)
                        next.append(v)

            frontier = next
            level += 1

        return visited





